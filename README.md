# PIC 10C, Spring 2017, Homework 1

## Siete y Medio Game

This is a program designed to play Siete y Medio, a card game similar to Blackjack, against you.

In Siete y Medio, as the name indicates, the goal is to continually draw cards and get as close to a total of 7.5 without going over. 
Going over 7.5 is called "busting," and it loses you the round. There is a dealer drawing cards at the same time as you, with the same goal as you:
get as close as possible to 7.5 without going over. Each round, the player bets a certain amount of money, and wins or loses that money based on the outcome.

### Outcomes

There are four possible outcomes to a round:

1. If the player comes closer to 7.5 than the dealer without busting, or the dealer busts, the player wins the round.
2. If the dealer comes closer to 7.5 than the player without busting, or the player busts, the player loses the round.
3. If both the player and the dealer bust, the player loses the round. This is called the "house advantage."
4. Both the player and the dealer have the same total, but neither of them busted. This is declared a tie, and no money is exchanged.

### The Deck

Siete y Medio is played with a 40-card deck, similar to the French deck but lacking 8s, 9s, or jokers. 
The four suits are **bastos** (clubs), **oros** (coins), **espadas** (spades/swords), and **copas** (cups).
The ranks are as follows:

* *As* (Ace, always worth 1)
* *Dos* (Two, worth 2)
* *Tres* (Three, worth 3)
* *Cuatro* (Four, worth 4)
* *Cinco* (Five, worth 5)
* *Seis* (Six worth 6)
* *Siete* (Seven, worth 7)
* *Sota* (Jack, rank 10, worth 0.5)
* *Caballo* (Knight, rank 11, worth 0.5)
* *Rey* (King, rank 12, worth 0.5)

### Gameplay

The game proceeds as such:

1. The player places a bet at the beginning of a round.
2. Both the player and the dealer draw one card from the deck. The dealer's hand is NOT visible to the player.
3. The player is asked whether they would like to draw another card repeatedly, until they stop or go over 7.5.
4. The dealer's play is always the same: they continue to draw cards only if the total is less than 5.5.
5. After the player's turn ends (either by the player's choice, or by them busting), the dealer's hand is revealed, the totals are compared and the winner is decided. Money is added to or taken from the player's total, and another round begins.
6. The game is over either when the player's total money goes down to 0, or when the dealer loses 900.

### Code Organization

All classes and member functions are declared in **cards.h**.  
All classes and member functions are defined in **cards.cpp**.  
The main routine, which plays the game and records the results, is defined in **siete\_y_medio.cpp**. 

### Instructions for Running

To play the game, simply compile and run, entering your bet as an integer at the prompt and signifying whether you want to take a card or stay with a 'y' or 'n'. 

After the game is over, a file called "gamelog.txt" will be saved to your local folder, which contains a recap of the game you just played. This file will be overwritten for each subsequent play, so you will need to move it elsewhere if you wish to maintain records beyond the most recent game.
