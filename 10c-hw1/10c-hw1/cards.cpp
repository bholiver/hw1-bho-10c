#include "cards.h"
#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <ctime>
#include <algorithm>
#include <iterator>

/*
You might or might not need these two extra libraries
#include <iomanip>
#include <algorithm>
*/


/* *************************************************
Card class
************************************************* */

/*
Default constructor for the Card class.
It could give repeated cards. This is OK.
Most variations of Blackjack are played with
several decks of cards at the same time.
*/
Card::Card() {
	int r = 1 + rand() % 4;
	switch (r) {
	case 1: suit = OROS; break;
	case 2: suit = COPAS; break;
	case 3: suit = ESPADAS; break;
	case 4: suit = BASTOS; break;
	default: break;
	}

	r = 1 + rand() % 10;
	switch (r) {
	case  1: rank = AS; break;
	case  2: rank = DOS; break;
	case  3: rank = TRES; break;
	case  4: rank = CUATRO; break;
	case  5: rank = CINCO; break;
	case  6: rank = SEIS; break;
	case  7: rank = SIETE; break;
	case  8: rank = SOTA; break;
	case  9: rank = CABALLO; break;
	case 10: rank = REY; break;
	default: break;
	}
}

// Accessor: returns a string with the suit of the card in Spanish 
std::string Card::get_spanish_suit() const {
	std::string suitName;
	switch (suit) {
	case OROS:
		suitName = "oros";
		break;
	case COPAS:
		suitName = "copas";
		break;
	case ESPADAS:
		suitName = "espadas";
		break;
	case BASTOS:
		suitName = "bastos";
		break;
	default: break;
	}
	return suitName;
}

// Accessor: returns a string with the rank of the card in Spanish 
std::string Card::get_spanish_rank() const {
	std::string rankName;
	switch (rank) {
	case AS:
		rankName = "As";
		break;
	case DOS:
		rankName = "Dos";
		break;
	case TRES:
		rankName = "Tres";
		break;
	case CUATRO:
		rankName = "Cuatro";
		break;
	case CINCO:
		rankName = "Cinco";
		break;
	case SEIS:
		rankName = "Seis";
		break;
	case SIETE:
		rankName = "Siete";
		break;
	case SOTA:
		rankName = "Sota";
		break;
	case CABALLO:
		rankName = "Caballo";
		break;
	case REY:
		rankName = "Rey";
		break;
	default: break;
	}
	return rankName;
}



// Accessor: returns a string with the suit of the card in English 
std::string Card::get_english_suit() const {
	std::string suitName;
	switch(suit) {
	case OROS:
		suitName = "coins";
		break;
	case COPAS:
		suitName = "cups";
		break;
	case ESPADAS:
		suitName = "spades";
		break;
	case BASTOS:
		suitName = "clubs";
		break;
	default: break;
	}
	return suitName;
}

// Accessor: returns a string with the rank of the card in English 
std::string Card::get_english_rank() const {
	std::string rankName;
	switch(rank) {
	case AS:
		rankName = "Ace";
		break;
	case DOS:
		rankName = "Two";
		break;
	case TRES:
		rankName = "Three";
		break;
	case CUATRO:
		rankName = "Four";
		break;
	case CINCO:
		rankName = "Five";
		break;
	case SEIS:
		rankName = "Six";
		break;
	case SIETE:
		rankName = "Seven";
		break;
	case SOTA:
		rankName = "Jack";
		break;
	case CABALLO:
		rankName = "Knight";
		break;
	case REY:
		rankName = "King";
		break;
	default: break;
	}
	return rankName;
}



// Assigns a numerical value to card based on rank.
// AS=1, DOS=2, ..., SIETE=7, SOTA=10, CABALLO=11, REY=12
int Card::get_rank() const {
	return static_cast<int>(rank) + 1;
}

// Comparison operator for cards
// Returns TRUE if card1 < card2
bool Card::operator < (Card card2) const {
	return rank < card2.rank;
}



/* *************************************************
Hand class
************************************************* */

// Default Constructor
Hand::Hand()
	: cards(std::vector<Card>())
{}

// Mutator: Adds a card to the hand and sorts the hand in ascending order.
void Hand::add_card(Card new_card) {
	// Add the new card to your hand
	cards.push_back(new_card);
	// Sort the cards using the above-defined < operator for Card
	std::sort( cards.begin(), cards.end() );
	return;
}

// Accessor: Displays the contents of your hand to the console.
std::ostream& Hand::display_cards(std::ostream& out) const
{
	// For each card in your hand, display a tab, then the Spanish name, followed by a tab, followed by the English name in parentheses, followed by a newline.
	for (std::size_t i = 0; i < cards.size(); ++i) {
		out << "\t" << cards[i].get_spanish_rank() << " de " << cards[i].get_spanish_suit()
			<< "\t(" << cards[i].get_english_rank() << " of " << cards[i].get_english_suit() << ").\n";
	}
	return out;
}

// Accessor: Sums and returns the total value of your hand 
double Hand::get_value() const
{
	// Create a temporary total variable
	double total = 0;

	// For each card in the hand,
	for (std::size_t i = 0; i < cards.size(); ++i) {
		// If it's ace through 7, we can simply add its rank to the total
		if (cards[i].get_rank() <= 7)
			total += cards[i].get_rank();
		// Otherwise, it's a sota/caballo/rey, so we add 0.5
		else if (cards[i].get_rank() > 7)
			total += 0.5;
	}

	// Return the total value
	return total;
}



/* *************************************************
Player class
************************************************* */

// Default Constructor, initializes money to 0
Player::Player()
	: money(0)
{}

// Player Constructor, initializes money to inputted value
Player::Player(int m)
	: money(m)
{}

// Accessor: Returns the amount of money the player has.
int Player::get_money() const {
	return money;
}

// Mutator: Adds the indicated amount of money to the player's money total.
void Player::add(int mon) {
	money += mon;
	return;
}

// Mutator: Subtracts the indicated amount of money from the player's money total.
void Player::subtract(int mon) {
	money -= mon;
	return;
}
