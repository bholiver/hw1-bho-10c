#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"

// Global constants (if any)


// Non member functions declarations (if any)


// Non member functions implementations (if any)


// Stub for main
int main() {

	// Seed the random function
	srand(time(nullptr));
	
	// Create the player and the dealer (who are both of Player class)
	Player player(100);
	Player dealer(900); // When the dealer loses all 900, the game ends

	// Create bet variable (will be reset with new bet every round)
	int currentBet;
	// Create hit/stay variable
	char hit = 'y';
	
	// Create bools to flip when the game/round are over
	bool gameOver = false;

	// Create ofstream to output into text file with game recap
	std::ofstream fileOut("gamelog.txt");
	// "Game number" variable to increment for the purposes of gamelog
	int gameNumber = 1;

	while (!gameOver)
	{

		// Create player and dealer's hand; both are empty bc start of new round
		Hand playerHand;
		Hand dealerHand;

		// Display player's current money and ask for bet
		std::cout << "You have $" << player.get_money() << ". Enter bet: $";
		// Take bet input
		std::cin >> currentBet;
		std::cout << std::endl;

		// If they bet more than they have, tell them to try again
		if (currentBet > player.get_money()) {
			std::cout << "That's more money than you have! Try again.\n\n";
			continue;
		}

		// WRITING TO GAME LOG: ******************************************

		fileOut << "-----------------------------------------------\n\n";
		fileOut << "Game Number: " << gameNumber << "\t\t" << "Money Left: $" << player.get_money() << "\n";
		fileOut << "Bet: $" << currentBet << "\n\n";

		// END GAME LOG WRITING ******************************************

		// Deal the initial cards to the player and the dealer
		playerHand.add_card(Card());
		dealerHand.add_card(Card());

		// Display player's cards, tell player their total, and ask if they want another card
		std::cout << "Your cards:\n";
		playerHand.display_cards(std::cout);
		std::cout << "Your total is " << playerHand.get_value() << ". Do you want another card (y/n)? ";
		// Get player input
		std::cin >> hit;
		// Newline
		std::cout << std::endl;

		// If they said 'y', repeat above code until they either say 'n' or bust.
		while (hit == 'y' || hit == 'Y')
		{
			// Deal them a new card, show it to them, and display their cards
			Card newCardPl = Card();
			std::cout << "New card: \n\t" << newCardPl.get_spanish_rank() << " de " << newCardPl.get_spanish_suit()
				<< "\t (" << newCardPl.get_english_rank() << " of " << newCardPl.get_english_suit() << ").\n\n";
			playerHand.add_card(newCardPl);
			std::cout << "Your cards:\n";
			playerHand.display_cards(std::cout);

			// Check for player busting on the draw early, so that we don't ask them for another card
			if (playerHand.get_value() > 7.5) {
				std::cout << "Your total is " << playerHand.get_value() << ". You busted!" << std::endl;
				break;
			}

			// Display total and ask them if they want another card
			std::cout << "Your total is " << playerHand.get_value() << ". Do you want another card (y/n)? ";
			std::cin >> hit;
		}
			
		// Display dealer's hand
		std::cout << "Dealer's cards: ";
		dealerHand.display_cards(std::cout);
		std::cout << "The dealer's total is " << dealerHand.get_value() << ".\n\n";

		// While the total is less than 5.5, continue dealing cards to the dealer
		while (dealerHand.get_value() < 5.5)
		{
			// Add a new card, display it, then display his hand and the total
			Card newCardDe = Card();
			std::cout << "New card: \n\t" << newCardDe.get_spanish_rank() << " de " << newCardDe.get_spanish_suit()
				<< "\t (" << newCardDe.get_english_rank() << " of " << newCardDe.get_english_suit() << ").\n\n";
			dealerHand.add_card(newCardDe);
				
			std::cout << "Dealer's cards:\n";
			dealerHand.display_cards(std::cout);
			std::cout << "The dealer's total is " << dealerHand.get_value() << ".\n\n";
		}

		// WRITING TO GAME LOG: ******************************************

		fileOut << "Your Cards:\n";
		playerHand.display_cards(fileOut);
		fileOut << "Your Total: " << playerHand.get_value() << ".\n\n";

		fileOut << "Dealer's Cards:\n";
		dealerHand.display_cards(fileOut);
		fileOut << "Dealer's total is " << dealerHand.get_value() << ".\n\n";

		// END GAME LOG WRITING ******************************************

		/* CONDITION 1: Player Wins */
		// In order to win, the player must get a value of <= 7.5 (did not bust) while either getting a higher value than the dealer OR while the dealer busts
		if (playerHand.get_value() <= 7.5 && (playerHand.get_value() > dealerHand.get_value() || dealerHand.get_value() > 7.5))
		{
			// In this case, the player wins, and their bet is added to their total money and subtracted from the dealer's.
			std::cout << "You win " << currentBet << ".\n\n";
			player.add(currentBet);
			dealer.subtract(currentBet);

			// Add player's win status to game log
			fileOut << "Player won!\n\n";
		}

		/* CONDITION 2: Player Wins */
		// Because a bust always means the player loses, whether the dealer busts or not, we can put all player-busts in this condition.
		else if (playerHand.get_value() > 7.5 || playerHand.get_value() < dealerHand.get_value())
		{
			// In this case, the player loses, and their bet is subtracted from their total money (but not added to dealer's?)
			std::cout << "Too bad. You lose " << currentBet << ".\n\n";
			player.subtract(currentBet);

			// Add player's loss status to game log
			fileOut << "Player lost...\n\n";
		}

		/* CONDITION 3: Tie*/
		// Player and dealer must have the exact same value, with neither of them busting. 
		// (Being lazy and not checking dealer hand total, because if they're equal they both didn't bust)
		else if (playerHand.get_value() <= 7.5 && playerHand.get_value() == dealerHand.get_value())
		{
			// In this case, it's a tie, and no one loses or gains any money.
			std::cout << "Nobody wins!\n\n";

			// Add tie status to game log
			fileOut << "It was a tie!\n\n";
		}

		// Increment game number
		++gameNumber;

		// If the player or the dealer is out of money, end the game
		if (player.get_money() <= 0) {
			std::cout << "You have $0. Game over!\nCome back when you have more money.\n\n";
			gameOver = true;
		}
		else if (dealer.get_money() <= 0) {
			std::cout << "Congratulations. You beat the casino!\n\n";
			gameOver = true;
		}
	}

	// Game Over message
	std::cout << "Bye!";

	// Close file
	fileOut.close();
	
	std::cin.ignore();
	std::cin.get();

	return 0;
}